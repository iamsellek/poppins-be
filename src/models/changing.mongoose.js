const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Changing = new Schema({
  hasPoo: Boolean,
  hasPee: Boolean,
  notes: String,
  activity: String,
  createdAt: Date,
  updatedAt: Date
});

Changing.pre('save', function(next) {
  var now = new Date();

  this.updatedAt = now;

  if (!this.createdAt) {
    this.createdAt = now;
  }

  next();
});

exports = module.exports = mongoose.model('Changing', Changing);
