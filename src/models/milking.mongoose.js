const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Milking = new Schema({
  isLeftBreast: Boolean,
  isRightBreast: Boolean,
  isPumping: Boolean,
  isBreastFeeding: Boolean,
  measurement: String,
  notes: String,
  activity: String,
  createdAt: Date,
  updatedAt: Date
});

Milking.pre('save', function(next) {
  var now = new Date();

  this.updatedAt = now;

  if (!this.createdAt) {
    this.createdAt = now;
  }

  next();
});

exports = module.exports = mongoose.model('Milking', Milking);
