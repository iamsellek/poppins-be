const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Sleep = new Schema({
  lengthInMinutes: Number,
  notes: String,
  activity: String,
  createdAt: Date,
  updatedAt: Date
});

Sleep.pre('save', function(next) {
  var now = new Date();

  this.updatedAt = now;

  if (!this.createdAt) {
    this.createdAt = now;
  }

  next();
});

exports = module.exports = mongoose.model('Sleep', Sleep);
