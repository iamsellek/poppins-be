const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const Feeding = new Schema({
  isFormula: Boolean,
  isBreastmilk: Boolean,
  measurement: String,
  notes: String,
  activity: String,
  createdAt: Date,
  updatedAt: Date
});

Feeding.pre('save', function(next) {
  var now = new Date();

  this.updatedAt = now;

  if (!this.createdAt) {
    this.createdAt = now;
  }

  next();
});

exports = module.exports = mongoose.model('Feeding', Feeding);
