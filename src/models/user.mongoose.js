const mongoose = require('mongoose');

const Schema = mongoose.Schema;

const User = new Schema({
    email: String,
    name: String,
    babyName: String
});

exports = module.exports = mongoose.model('User', User);
