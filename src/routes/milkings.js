var express = require('express');
var router = express.Router();

var activityDbHelper = require('../helpers/activityDbHelper');
var Milking = require('../models/milking.mongoose');

router.get('/', function(req, res) {
  var promise = activityDbHelper.getAllActivitiesOfType(Milking);

  promise.then(function (milkings) {
    var json = JSON.parse(JSON.stringify(milkings));

    res.send(json);
  }, function (err) {
    res.status(500).send(err);
  });
});

router.get('/:milkingId', function(req, res) {
  var promise = activityDbHelper.getSingleActivityById(Milking, req.params.milkingId);

  promise.then(function (milkings) {
    var json = JSON.parse(JSON.stringify(milkings));

    res.send(json);
  }, function (err) {
    res.status(500).send(err);
  });
});

router.post('/', function (req, res) {
  var promise = activityDbHelper.postNewActivity(Milking, req.body)

  promise.then(function(milking) {
    var json = JSON.parse(JSON.stringify(milking));

    res.send(json);
  }, function (err) {
    console.log(500);
    res.status(500).send(err);
  });
});

module.exports = router;
