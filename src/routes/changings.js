var express = require('express');
var router = express.Router();

var activityDbHelper = require('../helpers/activityDbHelper');
var Changing = require('../models/changing.mongoose');

router.get('/', function(req, res) {
  var promise = activityDbHelper.getAllActivitiesOfType(Changing);

  promise.then(function (changings) {
    var json = JSON.parse(JSON.stringify(changings));

    res.send(json);
  }, function (err) {
    res.status(500).send(err);
  });
});

router.get('/:changingId', function(req, res) {
  var promise = activityDbHelper.getSingleActivityById(Changing, req.params.changingId);

  promise.then(function (changings) {
    var json = JSON.parse(JSON.stringify(changings));

    res.send(json);
  }, function (err) {
    res.status(500).send(err);
  });
});

router.post('/', function (req, res) {
  var promise = activityDbHelper.postNewActivity(Changing, req.body)

  promise.then(function(changing) {
    var json = JSON.parse(JSON.stringify(changing));

    res.send(json);
  }, function (err) {
    console.log(500);
    res.status(500).send(err);
  });
});

module.exports = router;
