var express = require('express');
var router = express.Router();

var activityDbHelper = require('../helpers/activityDbHelper');
var Changing = require('../models/changing.mongoose');
var Feeding = require('../models/feeding.mongoose');
var Milking = require('../models/milking.mongoose');
var Sleep = require('../models/sleep.mongoose');

router.get('/', function(req, res) {
  var promises = [];

  promises.push(activityDbHelper.getAllActivitiesOfType(Changing));
  promises.push(activityDbHelper.getAllActivitiesOfType(Feeding));
  promises.push(activityDbHelper.getAllActivitiesOfType(Milking));
  promises.push(activityDbHelper.getAllActivitiesOfType(Sleep));

  Promise.all(promises).then(function (activities) {
    var sortedActivities = {};
    sortedActivities.changings = activities[0];
    sortedActivities.feedings = activities[1];
    sortedActivities.milkings = activities[2];
    sortedActivities.sleeps = activities[3];

    var json = JSON.parse(JSON.stringify(sortedActivities));

    res.send(json);
  }, function (err) {
    res.status(500).send(err);
  });
});

module.exports = router;
