var express = require('express');
var router = express.Router();

var activityDbHelper = require('../helpers/activityDbHelper');
var Feeding = require('../models/feeding.mongoose');

router.get('/', function(req, res) {
  var promise = activityDbHelper.getAllActivitiesOfType(Feeding);

  promise.then(function (feedings) {
    var json = JSON.parse(JSON.stringify(feedings));

    res.send(json);
  }, function (err) {
    res.status(500).send(err);
  });
});

router.get('/:feedingId', function(req, res) {
  var promise = activityDbHelper.getSingleActivityById(Feeding, req.params.feedingId);

  promise.then(function (feedings) {
    var json = JSON.parse(JSON.stringify(feedings));

    res.send(json);
  }, function (err) {
    res.status(500).send(err);
  });
});

router.post('/', function (req, res) {
  var promise = activityDbHelper.postNewActivity(Feeding, req.body)

  promise.then(function(feeding) {
    var json = JSON.parse(JSON.stringify(feeding));

    res.send(json);
  }, function (err) {
    console.log(500);
    res.status(500).send(err);
  });
});

module.exports = router;
