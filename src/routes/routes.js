'use strict';

exports = module.exports = {
	index: require('./index'),
	users: require('./users'),
	changings: require('./changings'),
	feedings: require('./feedings'),
    milkings: require('./milkings'),
    sleeps: require('./sleeps'),
	activities: require('./activities')
};
