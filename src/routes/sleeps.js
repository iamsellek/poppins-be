var express = require('express');
var router = express.Router();

var activityDbHelper = require('../helpers/activityDbHelper');
var Sleep = require('../models/sleep.mongoose');

router.get('/', function(req, res) {
  var promise = activityDbHelper.getAllActivitiesOfType(Sleep);

  promise.then(function (sleeps) {
    var json = JSON.parse(JSON.stringify(sleeps));

    res.send(json);
  }, function (err) {
    res.status(500).send(err);
  });
});

router.get('/:sleepId', function(req, res) {
  var promise = activityDbHelper.getSingleActivityById(Sleep, req.params.sleepId);

  promise.then(function (sleeps) {
    var json = JSON.parse(JSON.stringify(sleeps));

    res.send(json);
  }, function (err) {
    res.status(500).send(err);
  });
});

router.post('/', function (req, res) {
  var promise = activityDbHelper.postNewActivity(Sleep, req.body)

  promise.then(function(sleep) {
    var json = JSON.parse(JSON.stringify(sleep));

    res.send(json);
  }, function (err) {
    console.log(500);
    res.status(500).send(err);
  });
});

module.exports = router;
