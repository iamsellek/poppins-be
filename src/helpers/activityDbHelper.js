'use strict';

var getAllActivitiesOfType = function (mongooseType) {
  return new Promise(function (resolve, reject) {
    mongooseType.find(function (err, activities) {
      if (err) {
        reject(err);
      }

      resolve(activities);
    }).sort({ createdAt: -1 });
  });
};

var getSingleActivityById = function (mongooseType, id) {
  return new Promise(function (resolve, reject) {
    mongooseType.find({ _id: id }, function (err, response) {
      if (err) {
        reject(err);
      }

      resolve(response);
    });
  });
};

var postNewActivity = function (mongooseType, body) {
  return new Promise(function (resolve, reject) {
    var newActivity = new mongooseType(body);

    newActivity.save(function(err, response) {
      if (err) {
        reject(err);
      }

      resolve(response);
    });
  });
};

exports = module.exports = {
  getAllActivitiesOfType: getAllActivitiesOfType,
  getSingleActivityById: getSingleActivityById,
  postNewActivity: postNewActivity
};
